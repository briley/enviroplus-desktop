/*!
=========================================================
        ** Enviro+ Desktop Dashboard **
=========================================================

MIT License

Copyright (c) 2022 dedSyn4ps3

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

***This project is built on a skeleton using components designed by Creative Tim***

Any use, re-use, and adaptations of this project and it's code shall include the above license as well
as credit for the components used that were adapted from Creative Tim's work...

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/


import React, { Component } from "react";
import { MDBCardImage } from 'mdb-react-ui-kit';
import header from '../assets/img/header.jpg'

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col,
  Label,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import PanelHeader from "components/PanelHeader/PanelHeader.js";
import CircularProgressWithLabel from './CircularProgress.js'
import { invoke } from '@tauri-apps/api/tauri'

class Dashboard extends Component {

  intervalID;
  refreshID;

  constructor(props) {
    super(props);
    this.state = {
      temp: 0, hum: 0, press: 0, co: 0, nh3: 0,
      pi_address: "", addressModalVisible: false, errorModalVisible: false
    }
    //this.callBackend = this.callBackend.bind(this);
  }

  componentDidMount() {
    this.callBackend();
  }

  componentWillUnmount() {
    clearTimeout(this.intervalID);
  }


  setAddressModalVisible = (visible) => {
    this.setState({ addressModalVisible: visible });
  }

  setErrorModalVisible = (visible) => {
    this.setState({ errorModalVisible: visible });
  }

  ///////////////////////////////////////////////////////
  //  Get stored network address for the Raspberry Pi  //
  ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////
  //  Make call to Rust backend to query the Pi's flask server for sensor values  //
  //////////////////////////////////////////////////////////////////////////////////

  callBackend() {
    let i = localStorage.getItem("pi_address");
    if (i === null) {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else if (i == "") {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else {
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'temperature' }).then(value => { this.setState({ temp: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'humidity' }).then(value => { this.setState({ hum: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'reducing' }).then(value => { this.setState({ co: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'ammonia' }).then(value => { this.setState({ nh3: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'pressure' }).then(value => { this.setState({ press: parseFloat(value) }) }).catch((e) => { console.error(e); this.setErrorModalVisible(true) });
      this.intervalID = setTimeout(this.callBackend.bind(this), 60000);
    }
  }

  render() {
    return (
      <>
        <ErrorModal />
        <WarningModal />
        <PanelHeader
          size="md"
          content={
            <MDBCardImage position='top' src={header} alt='...' style={{ marginTop: -200 }} />
          }
        />
        <div className="content" style={{ marginTop: -100 }}>
          <Row>
            <Col xs={12} md={4}>
              <Card className="card-chart h-100">
                <CardHeader>
                  <h5 className="card-category">BME-280</h5>
                  <CardTitle tag="h4">Temperature</CardTitle>
                </CardHeader>
                <CardBody style={{ marginLeft: '-25%' }}>
                  <div className="chart-area">
                    <CircularProgressWithLabel value={this.state.temp} symbol="°F" style={{ width: 200, height: 200 }} color="success" />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12} md={4}>
              <Card className="card-chart h-100">
                <CardHeader>
                  <h5 className="card-category">BME-280</h5>
                  <CardTitle tag="h4">Humidity</CardTitle>
                </CardHeader>
                <CardBody style={{ marginLeft: '-25%' }}>
                  <div className="chart-area">
                    <CircularProgressWithLabel value={this.state.hum} symbol="%" style={{ width: 200, height: 200 }} />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12} md={4}>
              <Card className="card-chart h-100">
                <CardHeader>
                  <h5 className="card-category">BME-280</h5>
                  <CardTitle tag="h4">Pressure</CardTitle>
                </CardHeader>
                <CardBody style={{ marginLeft: '-20%', marginTop: '15%' }}>
                  <div className="chart-area">
                    <Label style={{ fontSize: 42 }}>{this.state.press} hPa</Label>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row style={{ marginTop: 25 }}>
            <Card className="card-chart">
              <CardHeader>
                <h5 className="card-category">BME-280</h5>
                <CardTitle tag="h4">Additional Readings</CardTitle>
              </CardHeader>
              <CardBody style={{ marginLeft: '15%' }}>
                <div className="chart-area">
                  <Row>
                    <Col>
                      <CardHeader>
                        <CardTitle tag="h4">CO</CardTitle>
                      </CardHeader>
                      <CardBody >
                        <div className="chart-area">
                          <Label style={{ fontSize: 60 }}>{this.state.co} kO</Label>
                        </div>
                      </CardBody>
                    </Col>
                    <Col>
                      <CardHeader>
                        <CardTitle tag="h4">NH3</CardTitle>
                      </CardHeader>
                      <CardBody>
                        <div className="chart-area">
                          <Label style={{ fontSize: 60 }}>{this.state.nh3} kO</Label>
                        </div>
                      </CardBody>
                    </Col>
                  </Row>
                </div>
              </CardBody>
            </Card>
          </Row>
        </div>
      </>
    );
  }
}

export default Dashboard;

///////////////////////////////////////////////////////////////
//  Warning modal in case no Pi address stored for requests  //
///////////////////////////////////////////////////////////////

class WarningModal extends Dashboard {
  render() {
    const { addressModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={addressModalVisible} toggle={() => { this.setAddressModalVisible(!addressModalVisible); }}>
          <ModalHeader>Empty Pi Address</ModalHeader>
          <ModalBody>
            You currently do not have a backend Pi address stored to query sensor data
            from! <b>To fix this issue, make sure to click on the Settings tab and input
              the ip address (including the port number) of the device running the
              sensor backend server.</b>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setAddressModalVisible(!addressModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

///////////////////////////////////////////
//  Error modal in case requests fail    //
///////////////////////////////////////////

class ErrorModal extends Dashboard {
  render() {
    const { errorModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={errorModalVisible} toggle={() => { this.setErrorModalVisible(!errorModalVisible); }}>
          <ModalHeader>Sensor Request Error</ModalHeader>
          <ModalBody>
            An error occurred while attempting to retrieve values from the backend
            Raspberry Pi server. This is most likely due to the querying of an
            improper backend address. <b>Check your local network to see if your Pi has been
              assigned a new ip address</b>, then update it accordingly from the Settings tab.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setErrorModalVisible(!errorModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}