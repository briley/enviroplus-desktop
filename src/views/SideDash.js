/*!
=========================================================
        ** Enviro+ Desktop Dashboard **
=========================================================

MIT License

Copyright (c) 2023 dedSyn4ps3

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


import React, { Component } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
} from "reactstrap";

import PanelHeader from "components/PanelHeader/PanelHeader";
import { WebviewWindow } from '@tauri-apps/api/window'

class SideDash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dash_title: "",
      side_url: ""
    }
  }

  componentDidMount() {
    this.loadValues();
  }

  loadValues = () => {
    let i = localStorage.getItem("dash_title")

    if (i === null) {
      this.setState({ dash_title: "Side Dashboard" });
    } else {
      this.setState({ dash_title: JSON.parse(i) });
    }

    i = localStorage.getItem("side_url")

    if (i === null) {
      this.setState({ side_url: "" });
    } else {
      this.setState({ side_url: JSON.parse(i) });
    }
  }

  openWindow() {
    const webview = new WebviewWindow('sideDashWindow', {
      url: this.state.side_url,
      title: "Enviroplus Desktop - Side Dashboard"
    });

    webview.once('tauri://created', function () {
      console.log("New window opened successfully!");
    });

    webview.once('tauri://error', function (e) {
      console.log("Error while opening window: " + e);
    });
  }

  render() {
    return (
      <>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>{this.state.dash_title}</CardHeader>
                <CardBody>
                  <div className="ratio ratio-4x3">
                    <iframe
                      src={this.state.side_url}
                      title="Secondary Webdash"
                      allowfullscreen
                    ></iframe>
                  </div>
                  <Row style={{ justifyContent: 'center' }}>
                    <Button style={{ width: '25%', marginTop: 20 }} onClick={() => this.openWindow()}>
                      OPEN IN NEW WINDOW
                    </Button>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default SideDash;
