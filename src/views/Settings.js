/*!
=========================================================
        ** Enviro+ Desktop Dashboard **
=========================================================

MIT License

Copyright (c) 2022 dedSyn4ps3

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


import React, { Component } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";


import PanelHeader from "components/PanelHeader/PanelHeader";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
        dash_title: "",
        side_url: "",
        pi_address: ""
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.saveValues = this.saveValues.bind(this);
  }

  componentDidMount() {
    this.loadValues();
}

handleChange = (prop) => (event) => {
    this.setState({ ...this.state, [prop]: event.target.value });
};

saveValues = () => {
  localStorage.setItem("dash_title", JSON.stringify(this.state.dash_title));
  localStorage.setItem("side_url", JSON.stringify(this.state.side_url));
  localStorage.setItem("pi_address", JSON.stringify(this.state.pi_address));
}

loadValues = () => {
  let i = localStorage.getItem("dash_title")

  if (i === null) {
      this.setState({ dash_title: "Side Dashboard" });
  } else {
      this.setState({ dash_title: JSON.parse(i) });
  }

  i = localStorage.getItem("side_url")

  if (i === null) {
      this.setState({ side_url: "" });
  } else {
      this.setState({ side_url: JSON.parse(i) });
  }

  i = localStorage.getItem("pi_address")

  if (i === null) {
      this.setState({ pi_address: "" });
  } else {
      this.setState({ pi_address: JSON.parse(i) });
  }
}

handleSubmit() {
  this.saveValues()
  this.refreshPage()
}

refreshPage() {
  window.location.reload(false);
}

  render() {
    return (
      <>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card style={{height: '125%'}}>
                <CardHeader style={{marginTop: 20}}>
                  <h5 className="title">Current Configuration</h5>
                </CardHeader>
                <CardBody style={{marginTop: 30}}>
                  <Form>
                    <Row style={{marginLeft: 60}}>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                          <label>Secondary Dashboard Title</label>
                          <Input
                            value={this.state.dash_title}
                            onChange={this.handleChange('dash_title')} 
                            placeholder="Title"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="5" style={{marginLeft: 80}}>
                        <FormGroup>
                          <label>Embed URL</label>
                          <Input placeholder="URL" type="text" value={this.state.side_url} onChange={this.handleChange('side_url')} />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row style={{marginLeft: 60}}>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                          <label>Raspberry Pi Server Address</label>
                          <Input
                            value={this.state.pi_address}
                            onChange={this.handleChange('pi_address')} 
                            placeholder="<ip_address:port>"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col style={{marginLeft: '23%', marginTop: '2%'}}>
                        <FormGroup>
                          <Button color="success" onClick={this.handleSubmit}>Update</Button>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Settings;
