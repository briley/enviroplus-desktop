#!/bin/sh

###################################################
#                                                 #
#      Simple startup script to run project       #
#                                                 #
###################################################


# Start Gunicorn backend for the Flask Sensor Server
gunicorn --workers=2 -b :8080 app:app &