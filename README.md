# Enviro Plus Desktop
![Python Version](https://img.shields.io/badge/python-v3.10-8A2BE2?logo=python&logoColor=%23f067fc)
![Node Version](https://img.shields.io/badge/nodejs-v20.8.0-success?logo=nodedotjs)
![React Version](https://img.shields.io/badge/react-v18.2.0-informational?logo=react)
![Rust Version](https://img.shields.io/badge/rust-v1.74-nightly?logo=rust&logoColor=dbaf5e&label=rust-nightly&color=orange)

<br>

![Main Overview](https://gitlab.com/dedSyn4ps3/enviroplus-desktop/-/raw/main/screenshots/overview.png "Overview") 

# Overview
The goal of this project is to offer another exciting way to utilize the excellent Enviro Plus HAT from Pimoroni, while giving
each maker, teacher, student, or enthusiast the opportunity to dig into the source code to better understand the inner workings
of the application.

The various walkthroughs and code examples provided by Pimoroni are an outstanding way for educators and makers alike to get started
quickly using boards such as the Enviro Plus HAT, but what about next steps? What are the steps needed to go beyond command line 
scripts, and on to more UI-based applications? That's usually up to the maker to figure out since each scenario is different. 


**What's better than having to figure out these steps? How about having a next-level application like this to learn from!**

<br>

### Run It Wherever
For those who may not be familiar with this project, it is based on our Web UI project that was released not too long ago. While it was designed and intended to run only on a Raspberry Pi device, <b>this version runs as a standalone desktop app</b>. This means that you can run the Enviro-Plus Flask server on your Pi, <u>and view sensor data from this application running on you Windows or macOS laptop/desktop</u>!

<br>

## Building Blocks

This project utilizes React as the front end user interface, and Flask on the back end. The primary reason for using Flask, if it isn't obvious,
is that it's a Python server microframework. This works out great since the Enviro Plus uses a large Python codebase for easy programmatic 
operations.

The React front end is built on top of an excellent template coded by <a href="https://www.creative-tim.com">Creative Tim</a>, but only utilizes a fraction of the capablilities that the original template included...mainly because it wasn't needed for this particular application. <u>Anyone looking for a great starting point for  a React project should definitely check out their awesome templates!</u>

<br>

**NEW FOR THIS PROJECT**
<br>

While your Raspberry Pi is still meant to run our Flask Sensor Server, it no longer needs to also run the frontend ui code! Instead, this standalone application will run on your laptop/desktop, and <b>it utilizes a Rust backend framework (Tauri) paired with the React front-end to offer and awesome browser-less experience!</b>

<br>

## Quick Start

- Make sure you have already gone through the setup process for the enviroplus-python repo install on your Raspberry Pi (**see below**)
- Copy the `start.sh` and `requirements.txt` from this repo to your Pi (using `scp` via `ssh` or any other means)
- `chmod +x start.sh`
- Install dependencies for the Flask Sensor Server on the Pi -> `python3 -m pip install -r requirements.txt`
- `./start.sh` to run a small script to get everything up and running!
- Run the `Enviroplus-Desktop` application on your laptop or desktop and update the `Pi Address` form field on the `Secondary Dashboard` tab

<br>

**IF YOU WANT TO EDIT AND BUILD THE PROJECT YOURSELF, MAKE SURE TO CLONE THIS ENTIRE REPO AND INSTALL ALL DEPENDENCIES USING THE `yarn` COMMAND FROM THE BASE DIRECTORY OF THE PROJECT**

<br>

## Troubleshooting
<u>It's important to keep the following points in mind when using this app</u>:

- If you are running a version of NodeJS > v17.* and want to edit and build the app yourself, you may need to adjust two `package.json` lines to the following because of changes to TLS/SSL implementations:

    - `"start": "cross-env BROWSER=none react-scripts --openssl-legacy-provider start"`

    - `"build": "react-scripts --openssl-legacy-provider build",`

- ~~**The application will immediately close if the Pi server is not running and able to process requests**~~
  > **UPDATE:** Recent changes to backend functionality now includes a **`match`** statement to handle any failed requests. The application will continue to run as expected, but will read "**0**" if data retrieval fails...

- It is recommended that you assign a Static IP Address to your Raspberry Pi to prevent connection issues should the Pi
be assigned a new IP via your local DHCP service

- If your Raspberry Pi happens to be assigned a new IP, ~~the desktop application will no longer launch properly (because it's trying to connect to an invalid address).~~ the application will continue to display "**0**" for each reading. To fix this, simply navigate to the **`SETTINGS`** tab and update the `endpoint address` for the Pi

![Secondary Dashboard](https://gitlab.com/dedSyn4ps3/enviroplus-desktop/-/raw/main/screenshots/settings.png "Secondary Dashboard") 

**REMOVING PREVIOUSLY STORED SETTINGS**
- The local storage folder to remove will be located in the following locations:

    - Windows: `C:\Users\<username>\AppData\Local\com.enviroplus.desktop\EBWebView\Default\Local Storage\leveldb`
    - macOS:   `/Users/<username>/Library/WebKit/com.enviroplus.desktop/WebsiteData/LocalStorage/`

<br>

## Enviro+ Install Script For Raspberry Pi

```bash
curl -sSL https://get.pimoroni.com/enviroplus | bash
```

### Alternative -> Install and Configure from GitHub

* `git clone https://github.com/pimoroni/enviroplus-python`
* `cd enviroplus-python`
* `sudo ./install.sh`

**Note:** Raspbian/Raspberry Pi OS Lite users may first need to install git: `sudo apt install git`

<br>

## Resources
- Web UI Project - https://gitlab.com/dedSyn4ps3/enviroplus-web-ui
- Learning Guides - https://learn.pimoroni.com/
- Support Forums - https://forums.pimoroni.com/c/support
- Other Services - https://www.nullreturn-it.com
- Creative Tim - https://demos.creative-tim.com/

 <h3>Support & Other Questions</h3>

 If you have questions or would like more info on how to set up your project, feel free to [reach out](mailto:support@nullsecurity.tech).

<br>

## Licensing

- Copyright 2023 dedSyn4ps3 (github.com/dedsyn4ps3)
- Licensed under MIT

### This project is built on a skeleton using components designed by Creative Tim

*Any use, re-use, and adaptations of this project and it's code shall include the above license as well as credit for the components used that were adapted from Creative Tim's work...*

- Copyright 2023 Creative Tim (https://www.creative-tim.com)
- Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/main/LICENSE.md)
