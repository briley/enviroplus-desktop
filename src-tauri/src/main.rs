//! # Enviroplus Desktop Backend
//!
//! `main` is the primary entrypoint for the backend server,
//! which calls the `generate_handler` function to create a
//! handle for `get_data` helper used to retrieve sensor data.
//!```
//!fn main() {
//!    tauri::Builder::default()
//!        .invoke_handler(tauri::generate_handler![get_data])
//!        .run(tauri::generate_context!())
//!        .expect("error while running tauri application");
//!}
//!
//!#[tauri::command]
//!async fn get_data(address: String, endpoint: String) -> String {
//!    let request_url = format!("http://{}/{}", address, endpoint);
//!
//!    let handle = thread::spawn(move || {
//!        let r = match query(request_url) {
//!            // If function returned OK...
//!            Ok(result) => result, // Otherwise...
//!            Err(_) => "0.0".into(),
//!        };
//!
//!        return r;
//!    });
//!
//!    let res = handle.join().unwrap();
//!
//!    return res
//!}
//! ```

#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use reqwest;
use std::thread;

/// Makes an asynchronous request to the provided
/// url using the `reqwest` crate.
///
/// Status code is checked for successful return
/// value. If request succeeds, value is returned to
/// the calling function.
///
/// # Example
///
/// ```
/// let response = reqwest::get(&url).await?;
/// match res.status() {
///     reqwest::StatusCode::OK => {
///         let body = res.text().await?;
///         return Ok(body);
///     }
///     _ => {
///         return Ok("Query failed".to_string());
///     }
/// };
/// ```
#[tokio::main]
async fn query(url: String) -> Result<String, reqwest::Error> {
    let res = reqwest::get(&url).await?;

    match res.status() {
        reqwest::StatusCode::OK => {
            let body = res.text().await?;
            return Ok(body);
        }
        _ => {
            return Ok("Query failed".to_string());
        }
    };
}

/// ### Primary backend function exposed to and callable by the user interface
///
/// Receives argument input from frontend form. A formatted
/// string is created using this input, **and then passed to the
/// `query` function to make the actual network request.**
///
/// By creating a thread handle, the function can be called multiple
/// times by the frontend and continue making `async` requests without
/// any overlap or blockage:
///
/// ```
/// let handle = thread::spawn(move || {
///     let r = match query(request_url) {
///         // If function returned OK...
///         Ok(result) => result, // Otherwise...
///         Err(_) => "0.0".into(),
///     };
///     
///     return r;
/// });
/// ```
#[tauri::command]
async fn get_data(address: String, endpoint: String) -> String {
    let request_url = format!("http://{}/{}", address, endpoint);

    let handle = thread::spawn(move || {
        let r = match query(request_url) {
            // If function returned OK...
            Ok(result) => result, // Otherwise...
            Err(_) => "0.0".into(),
        };

        return r;
    });

    let res = handle.join().unwrap();

    return res;
}

/// # Where the magic happens!
///
/// Calls the __default__ function of the `<Builder>` struct to create our
/// **Tauri** backend.
///
/// Uses `generate_handler` to bind any exposed backend functions in order
/// to be callable by the frontend user interface.
///
/// ## Example
/// ```
/// tauri::Builder::default()
///     .invoke_handler(tauri::generate_handler![some_function, another_function, and_another])
///     .run(tauri::generate_context!())
///     .expect("Uh oh, there was an issue running the application!");
/// ```
fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![get_data])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
